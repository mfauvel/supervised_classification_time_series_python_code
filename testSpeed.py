from IrSGP_model import IrSGP
import scipy as sp
import pandas as pd
import datetime as dt
import os

from joblib import Parallel, delayed

from sklearn.model_selection import train_test_split
from sklearn.gaussian_process.kernels import RBF, WhiteKernel, ExpSineSquared
from sklearn.gaussian_process.kernels import ConstantKernel as CK

import time

# Score metrics
# from sklearn.metrics import accuracy_score, cohen_kappa_score
# from sklearn.metrics import f1_score, precision_score, recall_score

# Get dates
timeSample = sp.loadtxt("/home/mfauvel/Documents/Recherche/INRA/These/Constantin/Codes/SimuFormosat2/Data/SITS/dates_formosat.txt").astype(str)
dateSample = pd.DatetimeIndex([dt.datetime(int(t[:4]),
                                           int(t[4:6]),
                                           int(t[6:8]))
                               for t in timeSample])

timeSample = dateSample.to_julian_date()-dateSample.to_julian_date().min()

os.environ["OPENBLAS_NUM_THREADS"]="1"

# Load data
data = sp.load("/home/mfauvel/Documents/Recherche/INRA/These/Constantin/Codes/SimuFormosat2/Data/np_table_6_bands_gp.npz", allow_pickle=True)
X = data["X"]
C = data["C"]

nc = 17
nb = 6
n_sig = 50
n_test = 10

seed = 10
use_val = True

X_train, X_test, c_train, c_test = train_test_split(X, C,
                                                    train_size=nc*n_sig,
                                                    test_size=nc*n_test,
                                                    stratify=C,
                                                    random_state=seed)

# Parameters
n_basis_ = 7
tau = int(timeSample.max())
K = CK(1, constant_value_bounds=(1e-5, 1e5)) * \
    RBF(length_scale=100, length_scale_bounds=(1, 100)) \
    + WhiteKernel(noise_level=0.01,
                  noise_level_bounds=(1e-05, 100000.0))

model = IrSGP(kernel=K, n_basis_=n_basis_, base="exp",
              tau=tau, epsilon=0.000001)

model.fit(X_train, c_train, compute_likelihood=True)
for c in range(nc):
    print("Kernel for class {}".format(c))
    for b in range(nb):
        print(model.kernels["{}{}".format(c, b)])
    print("\n")

# print('prediction')

# results=Parallel(n_jobs=-1)(
#     delayed(model.predict)(X_test[i:i+1, :]) for i in range(X_test.shape[0]))

ts = time.time()
zp = model.predict(X_test)
print("processing time {}".format(time.time() - ts))
print(zp)
# print(accuracy_score(c_test, results))
# print(cohen_kappa_score(c_test, results))
# print(f1_score(c_test, results, average=None))


# Before modifications 
# Kernel for class 0
# 3.91**2 * RBF(length_scale=100) + WhiteKernel(noise_level=1e-05)
# 21.4**2 * RBF(length_scale=1) + WhiteKernel(noise_level=3.41e+03)
# 3.77**2 * RBF(length_scale=1) + WhiteKernel(noise_level=203)
# 90.3**2 * RBF(length_scale=6.05) + WhiteKernel(noise_level=0.000594)
# 26.2**2 * RBF(length_scale=1) + WhiteKernel(noise_level=2.04e+03)
# 0.0353**2 * RBF(length_scale=100) + WhiteKernel(noise_level=0.00235)


# Kernel for class 1
# 0.292**2 * RBF(length_scale=100) + WhiteKernel(noise_level=1e-05)
# 27.2**2 * RBF(length_scale=1) + WhiteKernel(noise_level=3.52e+03)
# 4.91**2 * RBF(length_scale=1) + WhiteKernel(noise_level=64.4)
# 79.4**2 * RBF(length_scale=5.19) + WhiteKernel(noise_level=0.000818)
# 29.2**2 * RBF(length_scale=1) + WhiteKernel(noise_level=2.26e+03)
# 0.00316**2 * RBF(length_scale=100) + WhiteKernel(noise_level=0.000251)


# Kernel for class 2
# 9.95**2 * RBF(length_scale=100) + WhiteKernel(noise_level=1e-05)
# 18**2 * RBF(length_scale=1) + WhiteKernel(noise_level=1.43e+03)
# 7.38**2 * RBF(length_scale=1) + WhiteKernel(noise_level=781)
# 67.6**2 * RBF(length_scale=1) + WhiteKernel(noise_level=3.85e+03)
# 25.5**2 * RBF(length_scale=1) + WhiteKernel(noise_level=4.04e+03)
# 0.0847**2 * RBF(length_scale=78.8) + WhiteKernel(noise_level=0.0182)


# Kernel for class 3
# 8.33**2 * RBF(length_scale=100) + WhiteKernel(noise_level=1e-05)
# 24.6**2 * RBF(length_scale=1) + WhiteKernel(noise_level=1.98e+03)
# 10.5**2 * RBF(length_scale=1) + WhiteKernel(noise_level=826)
# 103**2 * RBF(length_scale=5.91) + WhiteKernel(noise_level=0.0654)
# 33.6**2 * RBF(length_scale=1) + WhiteKernel(noise_level=3.95e+03)
# 0.0324**2 * RBF(length_scale=100) + WhiteKernel(noise_level=0.0198)


# Kernel for class 4
# 9.74**2 * RBF(length_scale=100) + WhiteKernel(noise_level=1e-05)
# 21.5**2 * RBF(length_scale=1) + WhiteKernel(noise_level=1.71e+03)
# 12.6**2 * RBF(length_scale=1) + WhiteKernel(noise_level=629)
# 68.3**2 * RBF(length_scale=1) + WhiteKernel(noise_level=3.92e+03)
# 47.5**2 * RBF(length_scale=1) + WhiteKernel(noise_level=5.14e+03)
# 0.122**2 * RBF(length_scale=59.4) + WhiteKernel(noise_level=0.0166)


# Kernel for class 5
# 7.26**2 * RBF(length_scale=100) + WhiteKernel(noise_level=1e-05)
# 22.5**2 * RBF(length_scale=1) + WhiteKernel(noise_level=1.77e+03)
# 13.8**2 * RBF(length_scale=1) + WhiteKernel(noise_level=1.15e+03)
# 42.7**2 * RBF(length_scale=1) + WhiteKernel(noise_level=3.85e+03)
# 28.4**2 * RBF(length_scale=1) + WhiteKernel(noise_level=2.21e+03)
# 0.0425**2 * RBF(length_scale=100) + WhiteKernel(noise_level=0.0176)


# Kernel for class 6
# 8.89**2 * RBF(length_scale=100) + WhiteKernel(noise_level=1e-05)
# 18.9**2 * RBF(length_scale=1) + WhiteKernel(noise_level=1.51e+03)
# 12.2**2 * RBF(length_scale=1) + WhiteKernel(noise_level=1.02e+03)
# 52.7**2 * RBF(length_scale=1) + WhiteKernel(noise_level=4.14e+03)
# 29.3**2 * RBF(length_scale=1) + WhiteKernel(noise_level=2.29e+03)
# 0.0758**2 * RBF(length_scale=100) + WhiteKernel(noise_level=0.0174)


# Kernel for class 7
# 4.05**2 * RBF(length_scale=100) + WhiteKernel(noise_level=1e-05)
# 16.8**2 * RBF(length_scale=1) + WhiteKernel(noise_level=1.34e+03)
# 5.64**2 * RBF(length_scale=1) + WhiteKernel(noise_level=623)
# 36.8**2 * RBF(length_scale=1.01) + WhiteKernel(noise_level=3.69e+03)
# 30**2 * RBF(length_scale=1) + WhiteKernel(noise_level=2.34e+03)
# 0.0379**2 * RBF(length_scale=52.7) + WhiteKernel(noise_level=0.0185)


# Kernel for class 8
# 9.33**2 * RBF(length_scale=100) + WhiteKernel(noise_level=1e-05)
# 17.9**2 * RBF(length_scale=1) + WhiteKernel(noise_level=1.38e+03)
# 13.2**2 * RBF(length_scale=1) + WhiteKernel(noise_level=927)
# 60.9**2 * RBF(length_scale=5.56) + WhiteKernel(noise_level=0.0077)
# 23**2 * RBF(length_scale=1) + WhiteKernel(noise_level=1.75e+03)
# 0.0349**2 * RBF(length_scale=8.87) + WhiteKernel(noise_level=0.0107)


# Kernel for class 9
# 0.00316**2 * RBF(length_scale=100) + WhiteKernel(noise_level=1e-05)
# 0.00316**2 * RBF(length_scale=100) + WhiteKernel(noise_level=1e-05)
# 0.00316**2 * RBF(length_scale=100) + WhiteKernel(noise_level=1e-05)
# 0.00316**2 * RBF(length_scale=100) + WhiteKernel(noise_level=1e-05)
# 0.00316**2 * RBF(length_scale=100) + WhiteKernel(noise_level=1e-05)
# 0.00316**2 * RBF(length_scale=100) + WhiteKernel(noise_level=1e-05)


# Kernel for class 10
# 6.22**2 * RBF(length_scale=100) + WhiteKernel(noise_level=1e-05)
# 42.7**2 * RBF(length_scale=1) + WhiteKernel(noise_level=4.47e+03)
# 9.69**2 * RBF(length_scale=1) + WhiteKernel(noise_level=329)
# 110**2 * RBF(length_scale=5.49) + WhiteKernel(noise_level=0.000946)
# 48.9**2 * RBF(length_scale=1) + WhiteKernel(noise_level=4.9e+03)
# 0.0732**2 * RBF(length_scale=99.3) + WhiteKernel(noise_level=0.00687)


# Kernel for class 11
# 6.32**2 * RBF(length_scale=100) + WhiteKernel(noise_level=1e-05)
# 25.6**2 * RBF(length_scale=1) + WhiteKernel(noise_level=2.05e+03)
# 11.2**2 * RBF(length_scale=1) + WhiteKernel(noise_level=365)
# 82.2**2 * RBF(length_scale=5.99) + WhiteKernel(noise_level=0.00039)
# 31.3**2 * RBF(length_scale=1) + WhiteKernel(noise_level=2.5e+03)
# 0.0799**2 * RBF(length_scale=100) + WhiteKernel(noise_level=0.00292)


# Kernel for class 12
# 3.43**2 * RBF(length_scale=100) + WhiteKernel(noise_level=1e-05)
# 17.3**2 * RBF(length_scale=100) + WhiteKernel(noise_level=1.05e+03)
# 5.1**2 * RBF(length_scale=1.02) + WhiteKernel(noise_level=130)
# 16.3**2 * RBF(length_scale=100) + WhiteKernel(noise_level=1.2e+03)
# 37.1**2 * RBF(length_scale=1.99) + WhiteKernel(noise_level=119)
# 0.146**2 * RBF(length_scale=100) + WhiteKernel(noise_level=0.0102)


# Kernel for class 13
# 7.19**2 * RBF(length_scale=100) + WhiteKernel(noise_level=1e-05)
# 30.5**2 * RBF(length_scale=3.74) + WhiteKernel(noise_level=0.0242)
# 11.6**2 * RBF(length_scale=1) + WhiteKernel(noise_level=342)
# 13.5**2 * RBF(length_scale=1) + WhiteKernel(noise_level=565)
# 8.55**2 * RBF(length_scale=1.01) + WhiteKernel(noise_level=221)
# 0.0342**2 * RBF(length_scale=5.03) + WhiteKernel(noise_level=0.00137)


# Kernel for class 14
# 2.9**2 * RBF(length_scale=100) + WhiteKernel(noise_level=1e-05)
# 10.9**2 * RBF(length_scale=4.31) + WhiteKernel(noise_level=0.0026)
# 3.34**2 * RBF(length_scale=1) + WhiteKernel(noise_level=31.6)
# 3.55**2 * RBF(length_scale=1) + WhiteKernel(noise_level=67.1)
# 23.6**2 * RBF(length_scale=3.23) + WhiteKernel(noise_level=0.000456)
# 0.0966**2 * RBF(length_scale=94.1) + WhiteKernel(noise_level=0.00954)


# Kernel for class 15
# 10.2**2 * RBF(length_scale=100) + WhiteKernel(noise_level=1e-05)
# 16.7**2 * RBF(length_scale=1) + WhiteKernel(noise_level=1.24e+03)
# 10.3**2 * RBF(length_scale=1) + WhiteKernel(noise_level=779)
# 70**2 * RBF(length_scale=1) + WhiteKernel(noise_level=4.13e+03)
# 22**2 * RBF(length_scale=1) + WhiteKernel(noise_level=3.65e+03)
# 0.0427**2 * RBF(length_scale=100) + WhiteKernel(noise_level=0.0113)


# Kernel for class 16
# 7.51**2 * RBF(length_scale=100) + WhiteKernel(noise_level=1e-05)
# 29.1**2 * RBF(length_scale=1) + WhiteKernel(noise_level=3.75e+03)
# 12.4**2 * RBF(length_scale=1) + WhiteKernel(noise_level=409)
# 97.6**2 * RBF(length_scale=5.59) + WhiteKernel(noise_level=0.00121)
# 37.1**2 * RBF(length_scale=1) + WhiteKernel(noise_level=4.15e+03)
# 0.0822**2 * RBF(length_scale=100) + WhiteKernel(noise_level=0.00799)


# processing time 31.66592001914978
# [10  2  5  2 16  6  3  2  2  6  3 16  5  7  2  3  5  2  2  5  5  6  0  6
#  16  5  6  0  3  0  8  3  6  6  3 16 10  3 16  5  2  2  3  0  0  2  3 16
#   3 11  6  5 11  5  2  2  5 16 10  4  5  2 11  8  2 16  3 10  5  5  2  5
#   6 10  2  2  5  2  3  2  3  5  3 12  5  6  0  3  5 16 15  6 12  5 16  5
#  11 16  2  6  3  2  7  7  6  6  7 11  2  2 16 16  3  3  8 16 16  5  3 11
#  10 16 14  2  2  2  4  6 16  2  2  6  6  4 16  6 10  4  5 16  5 14  2 11
#   6  7  2  0  7 10  0  5 16  6  4 14  3  3  6 16  5 16  6  4  0 16  6  3
#   2  0]
