# coding: utf-8

import numpy as np

from scipy.special import kv, gamma
from scipy.spatial.distance import pdist, cdist, squareform

from sklearn.gaussian_process.kernels import Kernel, Hyperparameter, _approx_fprime



class SpectralMixture(Kernel):
    r"""
    Contains the Spectral Mixture kernel.
    Parameters
    ----------
    Q : Integer, default = 10
        The number of components in the mixture.
        It is set to "fixed" and cannot be changed during hyperparameter tuning.
    w : float or list, default = 1.0
        The weights for each component (Q).
    v : float or list, default = 1.0
        The variance for each component (Q).
    m : float or list, default = 1.0
        The mean for each component (Q).

    Examples
    --------
    """
    def __init__(self, Q = 10, w=1.0, v=1.0, m=1.0,
                 w_bounds=(1e-5, 1e5), v_bounds=(1e-5, 1e5), m_bounds=(1e-5, 1e5)):
        self.Q = int(Q)
        if isinstance(w, list):
            assert int(Q) == w.shape[0]
            assert int(Q) == v.shape[0]
            assert int(Q) == m.shape[0]

            self.w = np.asarray(w)
            self.v = np.asarray(v)
            self.m = np.asarray(m)
        else:
            self.w = w
            self.v = v
            self.m = m
        # Bounds
        self.w_bounds = w_bounds
        self.v_bounds = v_bounds
        self.m_bounds = m_bounds

    @classmethod
    def construct(cls, Q = 10, w=1.0, v=1.0, m=1.0,
                  w_bounds=(1e-5, 1e6), v_bounds=(1e-5, 1e5), m_bounds=(1e-5, 1e5)):
        if Q > 1 and len(np.atleast_1d(w)) == 1:
            w = np.repeat(v, Q)
            w = w / sum(w)
            w_bounds = np.vstack([w_bounds] *Q)
            v = np.repeat(v, Q)
            v_bounds = np.vstack([v_bounds] *Q)
            m = np.repeat(m, Q)
            m_bounds = np.vstack([m_bounds] *Q)
        return cls(Q, w, v, m, v_bounds, m_bounds)

    @property
    def hyperparameter_w(self):
        if self.Q == 1:
            return Hyperparameter(
                "w", "numeric", self.w_bounds)
        return Hyperparameter(
            "w", "numeric", self.w_bounds, self.w.shape[0])

    @property
    def hyperparameter_v(self):
        if self.Q == 1:
            return Hyperparameter(
                "v", "numeric", self.v_bounds)
        return Hyperparameter(
            "v", "numeric", self.v_bounds, self.v.shape[0])

    @property
    def hyperparameter_m(self):
        if self.Q == 1:
            return Hyperparameter(
                "m", "numeric", self.m_bounds)
        return Hyperparameter(
            "m", "numeric", self.m_bounds, self.m.shape[0])

    def __call__(self, X, Y=None, eval_gradient=False):
        """Return the kernel k(X, Y) and optionally its gradient.
        Parameters
        ----------
        X : ndarray of shape (n_samples_X, n_features)
            Left argument of the returned kernel k(X, Y)
        Y : ndarray of shape (n_samples_Y, n_features), default=None
            Right argument of the returned kernel k(X, Y). If None, k(X, X)
            if evaluated instead.
        eval_gradient : bool, default=False
            Determines whether the gradient with respect to the kernel
            hyperparameter is determined. Only supported when Y is None.
        Returns
        -------
        K : ndarray of shape (n_samples_X, n_samples_Y)
            Kernel k(X, Y)
        K_gradient : ndarray of shape (n_samples_X, n_samples_X, n_dims), \
                optional
            The gradient of the kernel k(X, X) with respect to the
            hyperparameter of the kernel. Only returned when `eval_gradient`
            is True.
        """
        X = np.atleast_2d(X)
            
        if Y is None:
            dists = squareform(pdist(X, lambda u, v: (u-v)))#metric='euclidean'))
            arg = 2 * np.pi * dists
            arg_sq = np.pi * arg * dists
            K = np.zeros((X.shape[0], X.shape[0]))
            if self.Q == 1:
                K = self.w * np.exp(- arg_sq * self.v) * np.cos(arg * self.m)
            else:
                for d in range(self.Q):
                    K += self.w[d] * np.multiply(np.exp(- arg_sq * self.v[d]),
                                                 np.cos(arg * self.m[d]))
        else:
            if eval_gradient:
                raise ValueError(
                    "Gradient can only be evaluated when Y is None.")
            dists = cdist(X, Y, lambda u, v: (u-v))#metric='euclidean')
            arg = 2 * np.pi * dists
            arg_sq = np.pi * np.multiply(arg, dists)
            K = np.zeros((X.shape[0], len(Y)))
            if self.Q == 1:
                K = self.w * np.multiply(np.exp(- 1 * arg_sq * self.v),
                                         np.cos(arg * self.m))
            else:
                for d in range(self.Q):
                    K += self.w[d] * np.multiply(np.exp(-1 * arg_sq * self.v[d]),
                                                 np.cos(arg * self.m[d]))

        if eval_gradient:
            w_gradient = np.empty((X.shape[0], X.shape[0], self.Q))
            v_gradient = np.empty((X.shape[0], X.shape[0], self.Q))
            m_gradient = np.empty((X.shape[0], X.shape[0], self.Q))

            # gradient with respect to each component d
            if self.Q == 1:
                w_gradient[:, :, 0] = np.exp(- arg_sq * self.v) * np.cos(arg * self.m)
                v_gradient[:, :, 0] = - 1 * self.w * arg_sq * np.exp(-1 * arg_sq * self.v) * \
                                      np.cos(self.m * arg)
                m_gradient[:, :, 0] = - self.w * arg * np.exp(-1 * arg_sq * self.v) * \
                                      np.sin(arg * self.m)
            else:
                for d in range(self.Q):
                    w_gradient[:, :, d] = np.exp(- arg_sq * self.v[d]) * np.cos(arg * self.m[d])
                    v_gradient[:, :, d] = - self.w[d] * arg_sq * np.exp(-1 * arg_sq * self.v[d])*\
                                          np.cos(self.m[d] * arg)
                    m_gradient[:, :, d] = - self.w[d] * arg * np.exp(-1 * arg_sq * self.v[d]) * \
                                          np.sin(arg * self.m[d])
                    
            if self.hyperparameter_w.fixed:
                w_gradient = np.empty((K.shape[0], K.shape[1], self.Q))
            if self.hyperparameter_v.fixed:
                v_gradient = np.empty((K.shape[0], K.shape[1], self.Q))
            if self.hyperparameter_m.fixed:
                m_gradient = np.empty((K.shape[0], K.shape[1], self.Q))
                    
            def f(theta):  # helper function
                return self.clone_with_theta(theta)(X)
            if self.Q == 1:
                weights = np.array([self.w, self.v, self.m])
                TT = 1 * np.exp(- arg_sq * self.v) * np.cos(arg * self.m)
            else:
                weights = np.concatenate((self.w, self.v, self.m), axis = -1)
                TT = np.multiply(np.exp(- arg_sq * self.v[0]),
                                 np.cos(arg * self.m[0]))
            weights = np.log(weights)
            K_gradient = _approx_fprime(weights, f, 1e-12)
            return K, K_gradient#np.dstack((m_gradient, v_gradient, w_gradient))
        else:
            return K

    def diag(self, X):
        """Returns the diagonal of the kernel k(X, X).
        The result of this method is identical to np.diag(self(X)); however,
        it can be evaluated more efficiently since only the diagonal is
        evaluated.
        Parameters
        ----------
        X : ndarray of shape (n_samples_X, n_features)
            Left argument of the returned kernel k(X, Y)
        Returns
        -------
        K_diag : ndarray of shape (n_samples_X,)
            Diagonal of kernel k(X, X)
        """
        return sum(self.w) * np.ones(X.shape[0])

    def __repr__(self):
        if self.Q == 1:
            param_str = "{0}(w={1}, v={2}, m={3})".format(
                self.__class__.__name__, self.w, self.v, self.m)
        else:
            param_str = "{0}(w=[{1}], v=[{2}], m=[{3}])".format(
                self.__class__.__name__,
                " ,".join(map("{0:.3g}".format, self.w)),
                " ,".join(map("{0:.3g}".format, self.v)),
                " ,".join(map("{0:.3g}".format, self.m)))
        return param_str

    
    def is_stationary(self):
        """Returns whether the kernel is stationary. """
        return True
