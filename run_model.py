#coding: utf-8

import os
import sys
import logging
sys.path.insert(0,'add_kernels')

import sqlite3


import pandas as pd
import numpy as np
import datetime as dt

# Classifiers
from IrSGP_model import IrSGP
# Parameters
from sklearn.gaussian_process.kernels import (RBF, WhiteKernel,
					      ExpSineSquared,
                                              RationalQuadratic,
                                              DotProduct, Matern)
from sklearn.gaussian_process.kernels import ConstantKernel as CK
from kernels_non_stationary import HeteroscedasticKernel as HWhiteKernel
# Score metrics
from sklearn.metrics import accuracy_score, cohen_kappa_score
from sklearn.metrics import f1_score

# Read inputs
tile = sys.argv[1]
num_db = int(sys.argv[2])
basis_ = sys.argv[3]
n_basis_ = int(sys.argv[4])

# Tile and parameters
if tile == "all":
    TILES = ["T31TCJ", "T31TDN", "T31TGK"]
else:
    TILES = [tile]
n_tiles = len(TILES)

# Wavelenghts (bands)
nFeatures = 10                          # Number of wavelengths
selected_feat = list(range(nFeatures))  # Select specific wavelenghts among nFeatures
nFeatures_selected = len(selected_feat)

# Additionnal parameters
rdn_state = 0
heterscedastic = True

if heterscedastic == False:
    K = CK(1, constant_value_bounds = (1e-3, 1e5))* \
          RBF(length_scale=100, length_scale_bounds = (1e-2, 1e5)) \
          + WhiteKernel(noise_level=0.01,
                        noise_level_bounds=(1e-05, 1e5))

# Datapath
npz_dataPath = "Data/"

# Functions
def read_npz_dataset(train_test = "train"):
    if train_test != "train":
        train_test = "val"
    
    for i in range(n_tiles):
        tile = TILES[i]

        # Read data
        data_ = np.load("{}{}_sub/data_{}_{}.npz".format(npz_dataPath, tile, 
                                                     train_test, num_db),
                        allow_pickle = True)

        X_read = data_['X']; X_gp_read = data_["X_gp"]
        C_read = data_['C']

        # If multiple tiles, concatenate
        if n_tiles > 1:            
            if i > 0:
                X_ = np.concatenate((X_, X_read), axis = 0)
                X_gp = np.concatenate((X_gp, X_gp_read), axis = 0)
                C_ = np.concatenate((C_, C_read), axis = 0)

        if i == 0:
            X_ = np.array(X_read)
            X_gp = np.array(X_gp_read)
            C_ = np.array(C_read)

    return X_, X_gp, C_

if __name__ == "__main__":
    ####################
    # Extract database #
    ####################
    X_train, X_train_gp, C_train = read_npz_dataset(
        train_test = "train")
    X_test, X_test_gp, C_test = read_npz_dataset(
        train_test = "test")
    
    ##################
    # Compute scores #
    ##################
    classes_, cnt_ = np.unique(C_train, return_counts = True)
    nc = classes_.shape[0]
    #for i_ in range(nc):
    #    cnt_data_per_classe[classes_[i_], 1] = cnt_[i_]
        
    # print(cnt_data_per_classe)
    nc = classes_.shape[0]
   
    # run Model
    if heterscedastic == True:
        # Heteroscedastic noise
        t_X = np.hstack(X_train[:, 0]).reshape(-1, 1)
        prototypes = np.quantile(t_X,
                                 np.linspace(0, 1, 30),
                                 interpolation="nearest").reshape(-1, 1)
        K = CK(1, constant_value_bounds=(1e-3, 1e5)) \
                                         * RBF(100,
                                               length_scale_bounds=(1e-05, 1e10)) \
                                               + HWhiteKernel.construct(prototypes,
                                                                        sigma_2 = 1,
                                                                        sigma_2_bounds=(1e-5, 1e2),
                                                                        gamma = 0.5, gamma_bounds="fixed")
    
    #K += CK() * Matern(length_scale=1.0, nu=2.5)
    tau = int(max(t_X))
    model = IrSGP(kernel=K, n_basis_=n_basis_, base = basis_,
                  tau=tau, epsilon=0.0001,
                  n_restarts_optimizer=0)

    # Train
    model.fit(X_train, C_train, compute_likelihood=True)
        
    # Test    
    results = model.predict(X_test)
    del(model)

    # Compute scores
    acc = accuracy_score(C_test, results)
    kappa = cohen_kappa_score(C_test, results)
    f1 = np.mean(f1_score(C_test, results, average = None))

    if n_tiles == 3:
        tile = "all"
    elif n_tiles == 2 and TILES[1] == "T31TDN":
        tile = "2_first"
    with open('results_{}_{}_{}_{}.txt'.format(tile, basis_, str(n_basis_), num_db), 'w') as fw:
        fw.write("Scores\n")
        fw.write(str(acc) + '\n')
        fw.write(str(kappa) + '\n')
        fw.write(str(f1) + '\n')

        
